﻿using System;
using Newtonsoft.Json;

namespace CosmosDbUpdater
{
    public class InvoiceArchiveItem
    {
        public string FileArchiveId { get; set; }
        public string CustomerId { get; set; }
        public string GlobalCustomerId { get; set; }
        public string Source { get; set; }
        public string InvoiceNumber { get; set; }
        public DateTime? InvoiceDate { get; set; }
        public DateTime? DueDate { get; set; }
        public string DistributionChannel { get; set; }
        public string EmailAddress { get; set; }
        public string AddressLine1 { get; set; }
        public string AddressLine2 { get; set; }
        public object PostalCode { get; set; }
        public object City { get; set; }
        public object CountryCode { get; set; }
        public string InvoiceType { get; set; }
        public string PaymentAccountNumber { get; set; }
        public string KidNumber { get; set; }
        public decimal? MinimumToPay { get; set; }
        public decimal? OutgoingBalance { get; set; }
        public string InvoiceArchiveType { get; set; }
        public string FileName { get; set; }
        public object InternalRefNumber { get; set; }
        public object TotalToPay { get; set; }
        public object ThreePercent { get; set; }
        public string PartitionKey { get; set; }
        public string id { get; set; }
        public string TypeName { get; set; }
        public string _etag { get; set; }
        public string _rid { get; set; }
        public string _self { get; set; }
        public string _attachments { get; set; }
        public int? _ts { get; set; }
    }
}
