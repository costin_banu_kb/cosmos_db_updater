﻿using Microsoft.Azure.Documents;
using Microsoft.Azure.Documents.Client;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace CosmosDbUpdater
{
    class Program
    {
        public static async Task Main(string[] args)
        {
            var config = new ConfigurationBuilder().AddUserSecrets<Program>().Build();
            var databaseName = config["DatabaseName"];
            var fileArchiveCollectionName = config["FileArchiveCollectionName"];
            var invoiceArchiveCollectionName = config["InvoiceArchiveCollectionName"];
            var endpointUrl = config["EndpointUrl"];
            var authorizationKey = config["AuthorizationKey"];

            using var client = new DocumentClient(new Uri(endpointUrl), authorizationKey);


            var doc = JsonConvert.DeserializeObject<Document>($@"
            {{
            	""FileArchiveId"": ""f25023f8-5d12-rf83-66fa-8523f8653eed"",
            	""CustomerId"": ""94dde736-bb6f-4eeb-8383-a8ae8c351a55"",
            	""GlobalCustomerId"": ""94dde736-bb6f-4eeb-8383-a8ae8c351a55"",
            	""Source"": ""CardSuite"",
            	""InvoiceNumber"": ""24005202"",
            	""InvoiceDate"": ""2020-07-31T00:00:00"",
            	""DueDate"": ""2026-07-20T00:00:00"",
            	""DistributionChannel"": ""POST"",
            	""EmailAddress"": ""shyamala.turpati@komplettbank.no"",
            	""AddressLine1"": ""LANGKAIA 1"",
            	""AddressLine2"": null,
            	""PostalCode"": ""0150"",
            	""City"": ""OSLO"",
            	""CountryCode"": ""NO"",
            	""InvoiceType"": ""C_P000"",
            	""PaymentAccountNumber"": ""6012.05.73953"",
            	""KidNumber"": ""102545123010283885"",
            	""MinimumToPay"": 2028.33,
            	""OutgoingBalance"": 35217.05,
            	""InvoiceArchiveType"": ""Invoice"",
            	""FileName"": ""purring.pdf"",
            	""InternalRefNumber"": ""24005202"",
            	""TotalToPay"": null,
            	""ThreePercent"": null,
            	""PartitionKey"": ""94dd"",
            	""id"": ""f25023f8-5d12-rf83-66fa-8523f8653eed"",
            	""TypeName"": ""InvoiceArchiveItem"",
            	""_etag"": ""\""2c00608e-0000-0d00-0000-5d77589e0000\"""",
            	""_rid"": ""lAwNAJZQc+PN6gQAAAAAAA=="",
            	""_self"": ""dbs/lAwNAA==/colls/lAwNAJZQc+M=/docs/lAwNAJZQc+PN6gQAAAAAAA==/"",
            	""_attachments"": ""attachments/"",
            	""_ts"": 1568102558
            }}

");


            //await client.CreateDocumentAsync(UriFactory.CreateDocumentCollectionUri(databaseName, collectionName), doc, null, true, default);



            ////query document
            var docs = client.CreateDocumentQuery<IDictionary<string, string>>(
                UriFactory.CreateDocumentCollectionUri(databaseName, fileArchiveCollectionName), new FeedOptions { EnableCrossPartitionQuery = true })
                          .Where(x => (x["JobInstanceId"] == "196dc309-e734-4162-9070-831c998d7634" || x["JobInstanceId"] == "e47b804c-e31f-404a-ac73-d618f2932682") && x["MimeType"] == "application/pdf")
                          .ToList()
                          .Select(x => x["CustomerId"]);

            var groups = from d in docs
                         group d by d into gr
                         select gr;

            using var dt = new DataTable("Stats");
            dt.Columns.AddRange(new[] { new DataColumn("CustomerId", typeof(string)), new DataColumn("Count", typeof(int)) });
            
            foreach (var g in groups)
            {
                var r = dt.NewRow();
                r["CustomerId"] = g.Key;
                r["Count"] = g.Count();
                dt.Rows.Add(r);
            }

            dt.WriteXml(@"c:\users\costin\desktop\stats.json", XmlWriteMode.WriteSchema);

            Console.WriteLine($"found {groups.Count(g => g.Count() > 4)} documents. processing...");

            ////update document
            //foreach (var doc in docs)
            //{
            //    doc.SetPropertyValue("CountryCode", "NO");
            //    await client.ReplaceDocumentAsync(doc);
            //}

            //delete document
            //foreach (var doc in docs)
            //{
            //    await client.DeleteDocumentAsync(doc.SelfLink, new RequestOptions { PartitionKey = new PartitionKey(doc.GetPropertyValue<string>("PartitionKey")) });
            //}

            Console.WriteLine("done");
            
        }
    }
}
